package br.com.garsystems.ccs.dto;

import java.io.Serializable;

public class CredenciaisDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String login;
	private String senha;
	private int id;
	
	public CredenciaisDTO() {}
	
	public CredenciaisDTO(String login, String senha, int id) {
		super();
		this.login = login;
		this.senha = senha;
		this.id = id;
	}
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
}