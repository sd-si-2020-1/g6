package br.com.garsystems.ccs.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.TableGenerator;

@Entity
public class Chat implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "CodeGenerator")
	@TableGenerator(table = "SEQUENCES_CHAT", name = "CodeGenerator")
	private int id;

	private String name;

	private String description;

	@OneToMany
	private List<ChatMessage> messages;

	@ManyToMany
	private List<User> users;
	
	public Chat() {}

	public Chat(String name, String description, List<ChatMessage> messages, List<User> users) {
		super();
		this.name = name;
		this.description = description;
		this.messages = messages;
		this.users = users;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<ChatMessage> getMessages() {
		return messages;
	}

	public void setMessages(List<ChatMessage> messages) {
		this.messages = messages;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

}
