package br.com.garsystems.ccs.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.garsystems.ccs.models.Chat;

public interface ChatRepository extends JpaRepository<Chat, Integer>{

}
