package br.com.garsystems.ccs.controllers;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.garsystems.ccs.models.User;

@Controller
@RequestMapping(value="/user")
public class UserController {

	@GetMapping("/getAll")
	public List<User> get() {
		return null;
	}
	
	@GetMapping("/{id}")
	public User getById(int id) {
		return null;
	}
	
	@PostMapping
	public ResponseEntity post(User user) {
		return null;
	}
	
	@PutMapping
	public ResponseEntity put(int id, User user) {
		return null;
	}
	
	@DeleteMapping
	public ResponseEntity delete(int id) {
		return null;
	}

}
