package br.com.garsystems.ccs.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.garsystems.ccs.controllers.StreamingResponseBodyController;
import br.com.garsystems.ccs.models.Chat;
import br.com.garsystems.ccs.models.ChatMessage;
import br.com.garsystems.ccs.repositories.ChatRepository;

@Service
public class ChatService {
	
	@Autowired 
	private StreamingResponseBodyController responseMessage;

	private ChatRepository repository;

	public void send(ChatMessage message) {
		responseMessage.handleRbe(message);
		Optional<Chat> chat = repository.findById(message.getChat().getId());
		chat.get().getMessages().add(message);
		repository.save(chat.get());
	}
	public Chat getById(int id) {
		return null;
	}

	public List<Chat> getAll() {
		return null;
	}

	public void edit(Chat chat) {

	}

	public void remove(int id) {

	}

}
