package br.com.garsystems.ccs.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import br.com.garsystems.ccs.models.ChatMessage;

@Controller
public class StreamingResponseBodyController {

    @GetMapping("/srb")
    public ResponseEntity<StreamingResponseBody> handleRbe(ChatMessage message) {
        StreamingResponseBody stream = out -> {
            out.write(message.getMessage().getBytes());
        };
        return new ResponseEntity(stream, HttpStatus.OK);
      }

}
