package br.com.garsystems.ccs.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.garsystems.ccs.models.Chat;
import br.com.garsystems.ccs.models.ChatMessage;
import br.com.garsystems.ccs.services.ChatService;

@Controller
@RequestMapping(value="/chat")
public class ChatController {
	
	@Autowired
	private ChatService service;
	
	@GetMapping("/sendMessage")
	public void send(ChatMessage message) {
		service.send(message);
	}

	@GetMapping("/getAll")
	public List<Chat> get() {
		return null;
	}

	@GetMapping("/{id}")
	public Chat getById(int id) {
		return null;
	}

	@PostMapping
	public ResponseEntity post(Chat chat) {
		return null;
	}

	@PutMapping
	public ResponseEntity put(int id, Chat chat) {
		return null;
	}

	@DeleteMapping
	public ResponseEntity delete(int id) {
		return null;
	}

}
