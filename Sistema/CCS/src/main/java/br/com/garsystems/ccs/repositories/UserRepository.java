package br.com.garsystems.ccs.repositories;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.garsystems.ccs.models.User;

public interface UserRepository extends JpaRepository<User, Integer>{

	@Transactional
	User findByUsername(String username);
}
