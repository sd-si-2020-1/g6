package br.com.garsystems.ccs.services;

import java.util.List;
import java.util.Optional;

import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.garsystems.ccs.models.User;
import br.com.garsystems.ccs.repositories.UserRepository;
import br.com.garsystems.ccs.security.UserSS;

@Service
public class UserService {
	
	@Autowired
	private UserRepository repository;
	
	public static UserSS authenticated() {
		try {
			return (UserSS) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		}
		catch (Exception e) {
			return null;
		}
	}
	
	@Transactional
	public User insert(User obj) {
		obj = repository.save(obj);
		return obj;
	}
	
	public User find(int id) {
		Optional<User> obj = repository.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + User.class.getName(), null));
	}

	public User getById(int id) {
		User user = find(id);
		return user;
	}

	public List<User> getAll() {
		List<User> users = repository.findAll();
		return users;
	}

	public void edit(User user) {
		find(user.getId());
		repository.save(user);
	}

	public void remove(int id) {
		find(id);
		repository.deleteById(id);
	}

}
